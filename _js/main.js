; (function ($) {

	'use strict';

	// var Scrollbar = window.Scrollbar;
	// Scrollbar.init(document.getElementById('main-scrollbar'), {
	// 	speed: 1,
	// 	overscrollEffectColor: '#FFE84F'
	// });

	$('.wrapper').imagesLoaded({
		background: true
	});

	var controller = new ScrollMagic.Controller({
		// refreshInterval: 300
	});

	/*=================================================
	=                   Header Back                   =
	=================================================*/
	function scrollTop() {

		const headerBack = $('.header__background');

		let scrollTopBack =	TweenMax.to(headerBack, 0.2, {
			autoAlpha: 0.9,
			ease: Power1.easeInOut
		});

		let scrollTopAction = new ScrollMagic.Scene({
			triggerElement: ".wrapper",
			triggerHook: 0.15,
			offset: 192
		})
		.setTween(scrollTopBack)
		.addTo(controller);
	}

	function scrollTopSearch() {

		const headerBack = $('.search__background');

		let scrollTopBack =	TweenMax.to(headerBack, 0.2, {
			autoAlpha: 0.9,
			ease: Power1.easeInOut
		});

		let scrollTopAction = new ScrollMagic.Scene({
			triggerElement: ".wrapper",
			triggerHook: 0.15,
			offset: 192
		})
		.setTween(scrollTopBack)
		.addTo(controller);
	}
	/*============  End of Header Back  =============*/

	/*===================================
	=            Menu                   =
	===================================*/

	const navToggle = $('#navToggle'),
		navMainDesktop = $('#header'),
		navMainDesktopLi = $('#navMainDesktop ul>li'),
		navToggleLine = $('.nav-toggle__line'),
		navToggleLineBefore = $('.nav-toggle__line--before'),
		navToggleLineAfter = $('.nav-toggle__line--after'),
		navToggleButton = $('#navToggle > .fa-bars'),
		wrapper = $('.wrapper'),
		headerMask = $('.header-mask'),
		headerLeftAfter = $('.header-left__bottom:before'),
		headerRightBefore = $('.header-right__bottom:before, .header-right__bottom:after'),
		headerRightClose = $('#navToggle > .fa-close')
	;

	const navToggleMobile = $('#navToggleMobile'),
		navMainMobile = $('#navMainMobile'),
		navMainMobileLi = $('#navMainMobile ul>li'),
		navBackgroundMobile = $('.header__background--mobile'),
		navToggleButtonMobile = $('#navToggleMobile > .fa-bars'),
		navToggleButtonCloseMobile = $('#navToggleMobile > .fa-close')
	;

	function navMenuOpen() {

		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(navMainDesktop, 0.1, {
				zIndex: 9910
			}, "-=0.8")
			.staggerFrom(navMainDesktopLi, 0.3, {
				yPercent: '-20%',
				// zIndex: 2,
				autoAlpha: 0,
				ease: Power1.easeInOut
			}, "0.1", "-=0.8")
			.to(navToggleButton, 0.4, {
				rotation: "-180deg",
				scale: '0',
				autoAlpha: 0,
				ease: Power1.easeInOut
			}, "-=0.6")
			.from(headerRightClose, 0.4, {
				autoAlpha: 0,
				rotation: "180",
				ease: Sine.easeInOut
			}, "-=0.4")
			// .to(navToggleLine, 0.4, {
			// 	autoAlpha: 0,
			// 	scale: 0,
			// 	ease: Power1.easeInOut
			// }, "-=0.8")
			// .to(navToggleLineBefore, 0.4, {
			// 	yPercent: "250%",
			// 	rotation: "45deg",
			// 	ease: Power1.easeInOut
			// }, "-=0.8")
			// .to(navToggleLineAfter, 0.4, {
			// 	yPercent: "-250%",
			// 	rotation: "-45deg",
			// 	ease: Power1.easeInOut
			// }, "-=0.8")
		;

		/*jshint -W030 */
		navToggle.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
		// wrapper.on('click', function () {
		//     tl.reverse();
		// });

	}

	function navMenuOpenMobile() {

		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(navBackgroundMobile, 0.4, {
				zIndex: 9890,
				autoAlpha: 1,
				ease: Power1.easeInOut
			}, "-=0.8")
			.to(navMainMobile, 0.4, {
				// zIndex: 9910,
				autoAlpha: 1,
				xPercent: -100,
				ease: Circ.easeInOut
			}, "-=1")
			.to(navToggleButtonMobile, 0.4, {
				rotation: "-180deg",
				scale: '0',
				autoAlpha: 0,
				ease: Power1.easeInOut
			}, "-=0.6")
			.from(navToggleButtonCloseMobile, 0.4, {
				autoAlpha: 0,
				rotation: "180",
				ease: Sine.easeInOut
			}, "-=0.4")
			// .staggerFrom(navMainMobileLi, 0.5, {
			// 	xPercent: '50%',
			// 	// zIndex: 2,
			// 	autoAlpha: 0,
			// 	ease: Circ.easeInOut
			// }, "0.03", "-=0.8")

			// .to(navToggleLine, 0.4, {
			// 	// autoAlpha: 0,
			// 	rotation: '-45deg',
			// 	scale: 1.1,
			// 	// visibility: 'visible',
			// 	ease: Power1.easeInOut
			// }, "-=0.8")
		;

		/*jshint -W030 */
		navToggleMobile.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
		navBackgroundMobile.on('click', function () {
			tl.reverse();
		});

	}

	/*=====  End of Menu Mobile  ======*/

	/*==============================
	=            Search            =
	==============================*/

	const searchToggle = $('#searchToggle'),
		searchBlock = $('#searchBlock'),
		searchHeader = $('#header'),
		headerSearchInput = $('.header-search__input'),
		headerSearchButton = $('.header-search__button'),
		headerSearchToggleIcon = $('#searchToggle > .fa-search'),
		headerSearchClose = $('#searchToggle > .fa-close')
	;

	const searchToggleMobile = $('#searchToggleMobile'),
		searchBlockMobile = $('#searchBlockMobile'),
		searchInputMobile = $('.header-search__input'),
		searchButtonMobile = $('.header-search__button'),
		searchToggleIconMobile = $('#searchToggleMobile > .fa-search'),
		searchCloseMobile = $('#searchToggleMobile > .fa-close'),
		searchToggleBackgroundMobile = $('.header-search__background--mobile')
	;

	function searchOpen() {

		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(searchHeader, 0.1, {
				zIndex: 9910
			}, "-=0.8")
			.from(searchBlock, 0.4, {
				autoAlpha: 0,
				scaleX: '0.97',
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "-=0.6")
			.staggerFrom([headerSearchInput, headerSearchButton], 0.6, {
				autoAlpha: 0,
				// xPercent: '5%',
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "0.1", "-=0.4")
			.to(headerSearchToggleIcon, 0.4, {
				autoAlpha: 0,
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "-=0.6")
			.from(headerSearchClose, 0.4, {
				autoAlpha: 0,
				rotation: "180",
				ease: Sine.easeInOut
			}, "-=0.4")
		;

		/*jshint -W030 */
		searchToggle.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
		// wrapper.on('click', function () {
		//     tl.reverse();
		// });

	}

	function searchOpenMobile() {

		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(searchBlockMobile, 0.4, {
				autoAlpha: 1,
				yPercent: '100%',
				zIndex: '9910',
				ease: Sine.easeInOut
			}, "-=0.6")
			.staggerFrom([searchInputMobile, searchButtonMobile], 0.6, {
				autoAlpha: 0,
				ease: Sine.easeInOut
			}, "0.1", "-=0.4")
			.to(searchToggleIconMobile, 0.4, {
				autoAlpha: 0,
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "-=0.6")
			.from(searchCloseMobile, 0.4, {
				autoAlpha: 0,
				rotation: "180",
				ease: Sine.easeInOut
			}, "-=0.4")
			.to(searchToggleBackgroundMobile, 0.4, {
				zIndex: 9890,
				autoAlpha: 1,
				ease: Power1.easeInOut
			}, "-=0.8")
		;

		/*jshint -W030 */
		searchToggleMobile.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
		searchToggleBackgroundMobile.on('click', function () {
			tl.reverse();
		});

	}

	/*=====  End of Search  ======*/

	/*==================================================
	=                   Category Nav                   =
	==================================================*/

	const categoryNavButton = $('#categoryNavButton'),
		categoryNav = $('.category-content__nav'),
		categoryNavNoactive = $('.category-content__nav > li'),
		categoryNavIcon = $('#categoryNavButton > i'),
		categoryRightContent = $('.category-content__right-content')
	;

	const categoryNavButtonMobile = $('#categoryNavButton'),
		categoryNavMobile = $('.category-content__nav'),
		categoryNavNoactiveMobile = $('.category-content__nav > li'),
		categoryNavIconMobile = $('#categoryNavButton > i'),
		categoryRightContentMobile = $('.category-content__right-content')
	;

	function categoryNavOpen() {
		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(categoryNav, 0.1, {
				overflow: 'visible',
				ease: Power1.easeInOut
			}, "-=0.6")
			.staggerFrom(categoryNavNoactive, 0.4, {
				autoAlpha: 0,
				yPercent: '-5%',
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "0.05", "-=0.6")
			.to(categoryNavIcon, 0.4, {
				rotation: '180',
				ease: Power1.easeInOut
			}, "-=0.4")
			.to(categoryRightContent, 0.4, {
				autoAlpha: 0,
				ease: Power1.easeInOut
			}, "-=1")
		;

		/*jshint -W030 */
		categoryNavButton.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
	}

	function categoryNavOpenMobile() {
		let tl = new TimelineMax({
			paused: true,
			reversed: true
		});

		tl
			.to(categoryNavMobile, 0.1, {
				overflow: 'visible',
				ease: Power1.easeInOut
			}, "-=0.6")
			.staggerFrom(categoryNavNoactiveMobile, 0.4, {
				autoAlpha: 0,
				yPercent: '-5%',
				// visibility: 'visible',
				ease: Sine.easeInOut
			}, "0.05", "-=0.6")
			.to(categoryNavIconMobile, 0.4, {
				rotation: '180',
				ease: Power1.easeInOut
			}, "-=0.4")
			.to(categoryRightContentMobile, 0.4, {
				autoAlpha: 0,
				ease: Power1.easeInOut
			}, "-=1")
		;

		/*jshint -W030 */
		categoryNavButtonMobile.on('click', function () {
			tl.reversed() ? tl.restart() : tl.reverse();
		});
	}

	/*============  End of Category Nav  =============*/

	/*==============================================
	=                   Category                   =
	==============================================*/

	function categoryContentOpen() {
		$('#categoryContentOpen').gridderExpander({
			scroll: true,
			scrollOffset: 450,
			scrollTo: "panel", // panel or listitem
			animationSpeed: 900,
			animationEasing: "easeInOutExpo",
			showNav: true, // Show Navigation
			nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
			prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
			closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
		});
	}

	function categoryContentOpen2() {
		$('#categoryContentOpen2').gridderExpander({
			scroll: true,
			scrollOffset: 360,
			scrollTo: "panel", // panel or listitem
			animationSpeed: 900,
			animationEasing: "easeInOutExpo",
			showNav: true, // Show Navigation
			nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
			prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
			closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
		});
	}

	function categoryContentOpenMobile() {
		$('#categoryContentOpen').gridderExpander({
			scroll: true,
			scrollOffset: 260,
			scrollTo: "panel", // panel or listitem
			animationSpeed: 900,
			animationEasing: "easeInOutExpo",
			showNav: true, // Show Navigation
			nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
			prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
			closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
		});
	}

	function categoryContentOpen2Mobile() {
		$('#categoryContentOpen2').gridderExpander({
			scroll: true,
			scrollOffset: 260,
			scrollTo: "panel", // panel or listitem
			animationSpeed: 900,
			animationEasing: "easeInOutExpo",
			showNav: true, // Show Navigation
			nextText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_next</title><polyline points=\"0 0 100 100 0 200\"/></svg>",
			prevText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 200\"><title>bm_prev</title><polyline points=\"100 200 0 100 100 0\"/></svg>",
			closeText: "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 200.71 200.71\"><title>bm_close</title><line x1=\"200\" y1=\"200\" x2=\"0\" y2=\"0\"/><line x1=\"200\" y1=\"0\" x2=\"0\" y2=\"200\"/></svg>"
		});
	}

	/*============  End of Category  =============*/

	function footerAdd() {
		$('.footer-container__copyright > p').text('<br>');
	}

	function initDesktop() {
		scrollTop();
		navMenuOpen();
		searchOpen();
		scrollTopSearch();
		// categoryNavOpen();
		// categoryContentOpen();
		// categoryContentOpen2();
	}

	function initMobile() {
		navMenuOpenMobile();
		searchOpenMobile();
		// categoryNavOpenMobile();
		// categoryContentOpenMobile();
		// categoryContentOpen2Mobile();
	}

	if (document.body.clientWidth > 420 || screen.width > 420) {
		initDesktop();
	} else {
		initMobile();
	}

})(jQuery);